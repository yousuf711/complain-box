import 'package:complain_box/src/helping/app_translation.dart';
import 'package:complain_box/src/repo/home_service.dart';
import 'package:complain_box/src/repo/pushNotification_service.dart';
import 'package:complain_box/src/repo/user_service.dart';
import 'package:complain_box/src/routes.dart';
import 'package:complain_box/src/view/root_screen.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:get/get.dart';

//
void main() async{
  WidgetsFlutterBinding.ensureInitialized();
  //...........................fcm source...........................
 // https://github.com/Amanullahgit/FCM_Flutter-Notification
  await Firebase.initializeApp();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  static final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
  @override
  Widget build(BuildContext context) {
    final pushNotificationService = PushNotificationService(_firebaseMessaging);
    pushNotificationService.initialise();

    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      translations: AppTranslation(),
      locale: Locale('en', 'BD'),// Get.locale
      fallbackLocale: Locale('en', 'US'),
      initialRoute: RootScreen.pageId,
      getPages: appPages,
    );
  }
}
//
//void main() async {
//  WidgetsFlutterBinding.ensureInitialized();
////  //...........................fcm source...........................
//// // https://github.com/Amanullahgit/FCM_Flutter-Notification
//  await Firebase.initializeApp();
//  return runApp(MaterialApp(home: MyApp()));
//}
//
//class MyApp extends StatefulWidget {
//  @override
//  State<StatefulWidget> createState() {
//    return MyAppState();
//  }
//}
//
//Future<dynamic> myBackgroundHandler(Map<String, dynamic> message) {
//  return MyAppState()._showNotification(message);
//}
//
//class MyAppState extends State<MyApp> {
//  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
//      FlutterLocalNotificationsPlugin();
//
//  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
//
//  @override
//  Widget build(BuildContext context) {
//    return Scaffold(
//      appBar: AppBar(
//        title: Text('notification'),
//      ),
//      body: Center(
//        child: Column(
//          children: [
//            Text('button'),
//            SizedBox(
//              height: 20,
//            ),
//            ElevatedButton(
//              onPressed: sendNotification,
//              child: Text('Send Notification'),
//            )
//          ],
//        ),
//      ),
//    );
//  }
// // f8AWHPkySZmJWLa6R0lht8:APA91bH-L018pIE1zlCIwFBn2360QOVRWYpiiErUWfSYGwuQaynetNAyR4s8Au64m-GGS0pRBozUd_3Yd0YdTRmfj6TNw_fc6ZHILGWY7ITD-n2h6GyQwHZoxQt4BjrdB-Cr4CJoYVgY
//  sendNotification() {
//    Map<String, dynamic> bodyMap = <String, dynamic>{
//      'body': "Notification body From Api",
//      'title': 'Notification title From Api'
////      'isImage': 'no',
////      'sender_phone': '01837005336',
////      'receiver_phone': '01821821971'
//    };
//    Map<String, dynamic> notificationMap = <String, dynamic>{
//      'to': '/topics/technician',
//      'collapse_key': 'type_a',
//      'notification': bodyMap,
//      'data': bodyMap
//    };
//
//    new ComplainService().sendNotification(notificationMap);
//  }
//
//  Future _showNotification(Map<String, dynamic> message) async {
//    var androidPlatformChannelSpecifics = new AndroidNotificationDetails(
//      'channel id',
//      'channel name',
//      'channel desc',
//      importance: Importance.max,
//      priority: Priority.high,
//    );
//    print(
//        'messge................................................................................ background');
//    var platformChannelSpecifics =
//        new NotificationDetails(android: androidPlatformChannelSpecifics);
//    await flutterLocalNotificationsPlugin.show(
//      0,
//      'new message arived',
//      'i want ${message['notification']['title']} for ${message['notification']['price']}',
//      platformChannelSpecifics,
//      payload: 'Default_Sound',
//    );
//  }
//
//  getTokenz() async {
//    String token = await _firebaseMessaging.getToken();
//    print(token);
//   //  FirebaseMessaging().subscribeToTopic('technician');
//  }
//
//  Future selectNotification(String payload) async {
//    await flutterLocalNotificationsPlugin.cancelAll();
//  }
//
//  @override
//  void initState() {
//    var initializationSettingsAndroid =
//        AndroidInitializationSettings('@mipmap/ic_launcher');
//
//    var initializationSettings =
//        InitializationSettings(android: initializationSettingsAndroid);
//    flutterLocalNotificationsPlugin.initialize(initializationSettings,
//        onSelectNotification: selectNotification);
//    super.initState();
//
//    _firebaseMessaging.configure(
//      onBackgroundMessage: myBackgroundHandler,
//      onMessage: (Map<String, dynamic> message) async {
//        print("onMessage: $message");
//        showDialog(
//            context: context,
//            builder: (context) {
//              return AlertDialog(
//                title: Text('new message arived'),
//                content: Text(
//                    // notification
//                    'i want ${message['notification']['title']} for ${message['notification']['body']}'),
//                actions: <Widget>[
//                  FlatButton(
//                    child: Text('Ok'),
//                    onPressed: () {
//                      Navigator.of(context).pop();
//                    },
//                  ),
//                ],
//              );
//            });
//      },
//    );
//
//    getTokenz();
//  }
//}
