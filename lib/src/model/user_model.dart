
import 'package:complain_box/src/mixins/mixins.dart';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class UserModel with PrintLogMixin {
  String id;
  String phone,name,email;
  bool isUser=true;

  UserModel({
    @required this.id,
    @required this.name,
    @required this.email,
    @required this.phone,
  });

   UserModel.fromDocumentSnapshot({DocumentSnapshot documentSnapshot}) {
    id = documentSnapshot.id;
    name = documentSnapshot["name"];
    email = documentSnapshot["email"];
    phone = documentSnapshot["phone"];
    isUser = documentSnapshot["isUser"];
  }
}
