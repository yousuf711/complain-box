

import 'package:cloud_firestore/cloud_firestore.dart';

class ComplainModel{
  String id,userId,name,phone,title,description,dateTime;

  ComplainModel({
      this.id, this.userId, this.name, this.phone, this.title, this.description, this.dateTime});


  ComplainModel.fromQueryDocumentSnapshot({QueryDocumentSnapshot queryDocSnapshot}) {
    var data = queryDocSnapshot.data();
    id = data["id"] ?? '';
    userId = data["userId"] ?? '';
    name = data["name"] ?? '';
    phone = data["phone"] ?? '';
    title = data["title"] ?? '';
    description = data["description"] ?? '';
    dateTime = data["date"];
  }

}