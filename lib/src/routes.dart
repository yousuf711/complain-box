import 'package:complain_box/src/binding/login_screen_binding.dart';
import 'package:complain_box/src/binding/root_screen_bindings.dart';
import 'package:complain_box/src/view/complain_screen.dart';
import 'package:complain_box/src/view/home_screen.dart';
import 'package:complain_box/src/view/login_screen.dart';
import 'package:complain_box/src/view/register_screen.dart';
import 'package:complain_box/src/view/root_screen.dart';
import 'package:complain_box/src/view/splash_screen.dart';
import 'package:get/get.dart';

import 'binding/home_screen_binding.dart';

final List<GetPage> appPages = [
  GetPage(
    name: RootScreen.pageId,
    page: () => RootScreen(),
      binding: RootScreenBindings(),
  ),
  GetPage(
    name: MySplashScreen.pageId,
    page: () => MySplashScreen(),
  ),
  GetPage(
    name: LoginScreen.pageId,
    page: () => LoginScreen(),
 //  binding: LoginScreenBinding(),
    transition: Transition.upToDown
  ),
  GetPage(
    name: RegisterScreen.pageId,
    page: () => RegisterScreen(),
//      binding: LoginScreenBinding(),
    transition: Transition.upToDown
  ),
  GetPage(
      name: HomeScreen.pageId,
      page: () => HomeScreen(),
    //  binding: HomeScreenBinding(),
      transition: Transition.upToDown
     ),
  GetPage(
      name: ComplainScreen.pageId,
      page: () => ComplainScreen(),
    //  binding: HomeScreenBinding(),
      transition: Transition.upToDown
     ),
];
