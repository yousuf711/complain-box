import 'package:flutter/material.dart';


import 'auto_size_text_widget.dart';

class ContactsCountWidget extends StatelessWidget {
  final int complainCount;

  ContactsCountWidget({this.complainCount});

  @override
  Widget build(BuildContext context) {
    return Container(
      child: AutoSizeTextWidget(
        text: '${complainCount ?? "0"} Complains',
        style: TextStyle(
          color: Colors.white,
          fontSize: 16,
          fontWeight: FontWeight.w400,
        ),
      ),
    );
  }
}
