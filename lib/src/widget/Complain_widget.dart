import 'package:complain_box/src/model/complain_model.dart';
import 'package:complain_box/src/widget/commons.dart';
import 'package:complain_box/src/widget/custom_text.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

class ComplainWidget extends StatelessWidget {
  ComplainModel complainModel;
  DateFormat formattedDate = DateFormat('yyyy-MM-dd  KK:mm a');

  ComplainWidget(this.complainModel);

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: EdgeInsets.all(5),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(5)),
        ),
        child: Card(
          elevation: 5,

          child: Align(
            alignment: Alignment.centerLeft,
            child: Padding(
              padding: EdgeInsets.only(left: 10,right: 5,top: 5,bottom: 5),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  CustomText(
                    text: complainModel.name.trim(),
                    color: black,
                    size: 22,
                    weight: FontWeight.bold,
                  ),
                  CustomText(
                    text: 'Phone : ' + complainModel.phone.trim(),
                    color: grey,
                    size: 16,
                    weight: FontWeight.normal,
                  ),
                  CustomText(
                    text: complainModel.title.trim(),
                    color: black,
                    size: 20,
                    weight: FontWeight.normal,
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  CustomText(
                    text: complainModel.description,
                    color: grey,
                    size: 16,
                    weight: FontWeight.normal,
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  //           +

                  CustomText(
                    text:
                        'Date : ${(complainModel.dateTime != null ? formattedDate.format(DateTime.parse(complainModel.dateTime)) : '')}',
                    color: grey,
                    size: 13,
                    weight: FontWeight.normal,
                  ),
                  SizedBox(
                    height: 5,
                  ),
                ],
              ),
            ),
          ),
        ));
  }
}
