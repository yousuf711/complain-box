

class AppStringKey{
  static const String app_name="Complainbox";
  static const String complainbox="complainbox";
  static const String password="password";
  static const String logout="logout";
  static const String edit_profile="edit_profile";

  static const String user_name="user_name";
  static const String name="name";
  static const String email="email";
  static const String complain_box_registation='Complain Box Registation';
  static const String user_registation='User Registation';
  static const String phone_number="phone_number";
  static const String registration="registration";
  static const String login="login";
  static const String leave_your_complain_here="leave_your_complain_here";
  static const String action_login="action_login";
  static const String mobile_number="mobile_number";
  static const String title="title";
  static const String re_password="re_password";
  static const String new_account="new_account";
  static const String description="description";
  static const String sendComplain="sendComplain";
  static const String all_complain="all_complain";
  static const String my_complain="my_complain";

}