import 'package:complain_box/src/helping/appStringFile.dart';
import 'package:get/get.dart';

class AppTranslation extends Translations {
  @override
  Map<String, Map<String, String>> get keys => {
        'en_US': {
          AppStringKey.registration: 'Registration',
          AppStringKey.login: 'Login',
          AppStringKey.password: 'Password',
          AppStringKey.logout: 'Log out',
          AppStringKey.edit_profile: 'Edit Profile',
          AppStringKey.user_name: 'User Name',
          AppStringKey.name: 'Name',
          AppStringKey.action_login: 'Login',
          AppStringKey.email: 'Email',
          AppStringKey.phone_number: 'Phone Number',
          AppStringKey.mobile_number: 'Mobile Number',
          AppStringKey.re_password: 'Re Password',
          AppStringKey.registration: 'Registration',
          AppStringKey.new_account: 'New Account? ',
          AppStringKey.complain_box_registation: 'Complain Box Registation',
          AppStringKey.user_registation: 'User Registation',
          AppStringKey.complainbox: 'Complain Box',
          AppStringKey.title: 'Title',
          AppStringKey.description: 'Description',
          AppStringKey.sendComplain: 'Send Complain',
          AppStringKey.leave_your_complain_here: 'Leave your complain here',
          AppStringKey.all_complain: 'All Complain',
          AppStringKey.my_complain: 'My Complain',
        },
        'bn_BD': {
          AppStringKey.registration: 'নিবন্ধন',
          AppStringKey.login: 'লগইন',
          AppStringKey.password: 'পাসওয়ার্ড',
          AppStringKey.logout: 'লগ আউট',
          AppStringKey.edit_profile: 'এডিট প্রোফাইল',
          AppStringKey.user_name: 'ইউজার নেম',
          AppStringKey.name: 'নাম',
          AppStringKey.email: 'ই-মেইল',
          AppStringKey.phone_number: 'মোবাইল নাম্বার',
          AppStringKey.mobile_number: 'মোবাইল নম্বর',
          AppStringKey.re_password: 'পাসওয়ার্ডটি পুনরায় দিন',
          AppStringKey.registration: 'রেজিস্ট্রেশন',
        },
      };
}
