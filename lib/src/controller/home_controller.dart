import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:complain_box/src/controller/notification_controller.dart';
import 'package:complain_box/src/helping/helpers.dart';
import 'package:complain_box/src/model/complain_model.dart';
import 'package:complain_box/src/model/user_model.dart';
import 'package:complain_box/src/repo/auth_service.dart';
import 'package:complain_box/src/repo/home_service.dart';
import 'package:complain_box/src/repo/user_service.dart';
import 'package:complain_box/src/util/status.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';

class HomeController extends GetxController {
  var status = Status.LOADING.obs;
  var isUser = true.obs;
  var isLoading = false.obs;
  var _complainService = ComplainService();

  RxInt _complainListCount = 0.obs;

  RxInt get complainListCount => _complainListCount;

  void setComplainListCount(int val) => _complainListCount.value = val;
  FirebaseAuth fireAuth = FirebaseAuth.instance;
  Rx<UserModel> userModel = new Rx<UserModel>();
  final AuthService _authService = AuthService();
  final UserService _userService = UserService();

  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();
  // getUser();
  }

  Stream<QuerySnapshot> getUser() {
    //status.value = Status.LOADING;
    //   isLoading.value = true;
    var doc = _userService.getUser(fireAuth.currentUser.uid);
    doc.then((value) => {
          userModel.value =
              UserModel.fromDocumentSnapshot(documentSnapshot: value),
          print('user .............................${value.data()['name']}'),
          print('user .............................${userModel.value.name}'),
          fireAuth.currentUser
              .getIdToken()
              .then((value) => print('token :          ${value.toString()}')),
          status.value = Status.SUCCESS,
          isLoading.value = false
        });
  }

  Future<void> addComplain(ComplainModel complainModel) async {
    status.value = Status.LOADING;
    isLoading.value = true;
    try {
      var _comModel = await _complainService.addComplain(complainModel);
      await Get.find<NotificationController>()
          .sendNotification(complainModel, userModel.value);
      if (_comModel != null) {
        status.value = Status.SUCCESS;
        isLoading.value = false;
        Fluttertoast.showToast(msg: 'Success');
      } else {
        status.value = Status.ERROR;
        isLoading.value = false;
      }
    } catch (e) {
      Helpers.showSnackbar(title: 'Error', message: e.message);
      status.value = Status.ERROR;
      isLoading.value = false;
    }
  }

  Stream<QuerySnapshot> fetchComplain() {
    status.value = Status.LOADING;
    isLoading.value = true;
    Stream<QuerySnapshot> qSnapStream = _complainService.fetchComplain();

    qSnapStream.forEach((QuerySnapshot qSnapItem) {
      setComplainListCount(qSnapItem.size);
    });
    status.value = Status.SUCCESS;
    isLoading.value = false;
    return qSnapStream;
  }

  Stream<QuerySnapshot> fetchMyComplain() {
//    status.value = Status.LOADING;
//    isLoading.value = true;
    Stream<QuerySnapshot> qSnapStream =
        ComplainService().fetchMyComplain(fireAuth.currentUser.uid);

    qSnapStream.forEach((QuerySnapshot qSnapItem) {
      setComplainListCount(qSnapItem.size);
    });
//    status.value = Status.SUCCESS;
//    isLoading.value = false;
    return qSnapStream;
  }
}
