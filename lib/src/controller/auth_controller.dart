import 'package:complain_box/src/controller/notification_controller.dart';
import 'package:complain_box/src/helping/helpers.dart';
import 'package:complain_box/src/mixins/mixins.dart';
import 'package:complain_box/src/model/user_model.dart';
import 'package:complain_box/src/repo/auth_service.dart';
import 'package:complain_box/src/repo/user_service.dart';
import 'package:complain_box/src/util/my_shared_preparence.dart';
import 'package:complain_box/src/util/shared_pref_key.dart';
import 'package:complain_box/src/util/status.dart';
import 'package:complain_box/src/view/home_screen.dart';
import 'package:complain_box/src/view/login_screen.dart';
import 'package:complain_box/src/view/root_screen.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';

class AuthController extends GetxController with PrintLogMixin {
  var status = Status.LOADING.obs;
  var isLoading = false.obs;
  FirebaseAuth _fireAuth = FirebaseAuth.instance;
  FirebaseMessaging firebaseMessaging = new FirebaseMessaging();

  var radioValue = 0.obs;

  final AuthService _authService = AuthService();
  final UserService _userService = UserService();

  Rx<User> _firebaseUser = Rx<User>();

  User get user => _firebaseUser.value;

  User setUser(User user) => _firebaseUser.value = user;

  @override
  onInit() {
    //  _firebaseUser.bindStream(_fireAuth.authStateChanges());
  }

  void handleRadioValueChange(int value) {
    radioValue.value = value;
    switch (radioValue.value) {
      case 0:
        break;
      case 1:
        break;
      case 2:
        break;
    }
  }

  void _createUser(UserCredential _authResult, String name, String email,
      String phone) async {
    try {
      UserModel user = UserModel(
        id: _authResult.user.uid,
        name: name,
        email: email,
        phone: phone,
      );
      var result = await _userService.createNewUser(user);
      printLog("_authResult $result");
      setUser(_fireAuth.currentUser);
      isLoading.value = false;
      status.value = Status.SUCCESS;
      Get.offNamed(LoginScreen.pageId);
    } catch (e) {
      printLog(e);
      Helpers.showSnackbar(title: 'Error', message: e.message);
      isLoading.value = false;
      status.value = Status.ERROR;
    }
  }

  Future<void> register(
      String phone, String email, String name, String password) async {
    isLoading.value = true;
    status.value = Status.LOADING;
    try {
      UserCredential _authResult =
          await _authService.registerUser(email.trim(), password.trim());

      printLog("_authResult $_authResult");

      if (_authResult != null && _authResult.user != null) {
        _createUser(_authResult, name, email, phone);
      } else {
        isLoading.value = false;
        status.value = Status.ERROR;
      }
    } catch (e) {
      printLog(e);
      Helpers.showSnackbar(title: 'Error', message: e.message);
      isLoading.value = false;
      status.value = Status.ERROR;
    }
  }

  Future<void> login(String email, String password) async {
    isLoading.value = true;
    status.value = Status.LOADING;
    try {
      UserCredential _authResult =
          await _authService.loginUser(email.trim(), password.trim());

      if (_authResult != null && _authResult.user != null) {
        var documentSnapshot = await _userService.getUser(_authResult.user.uid);
        //  _createUser(_authResult,email);
      }
      if (_authResult.user.uid != null) {
        UserModel userModel;
        var doc = _userService.getUser(_fireAuth.currentUser.uid);
        doc.then((value) => {
              userModel =
                  UserModel.fromDocumentSnapshot(documentSnapshot: value),
              if (userModel.isUser)
                {
                  if (radioValue.value == 0)
                    {
                      saveLoginData(email, password),
                    }
                  else
                    {
                      isLoading.value = false,
                      status.value = Status.ERROR,
                      _authService.signOutUser(),
                      Fluttertoast.showToast(msg: 'You are not a technician')
                    }
                }
              else {
                  if (radioValue.value == 1)
                    {
                      saveLoginData(email, password),
                    }
                  else
                    {
                      isLoading.value = false,
                      status.value = Status.ERROR,
                      _authService.signOutUser(),
                      Fluttertoast.showToast(msg: 'You are not a user')
                    },
                }
            });
        printLog("userid..........................${_authResult.user.uid}");
        printLog(
            "_fireAuth.currentUser.uid.......${_fireAuth.currentUser.uid}");
      } else {
        isLoading.value = false;
        status.value = Status.ERROR;
        Helpers.showSnackbar(
            title: 'Error', message: "User id or password incorrect");
      }
    } catch (e) {
      printLog(e);
      Helpers.showSnackbar(title: 'Error', message: e.message);
      isLoading.value = false;
      status.value = Status.ERROR;
    }
  }

  void signOutUser() async {
    try {
      await Get.find<NotificationController>().fcmUnSubscribe();
      await _authService.signOutUser();
      Get.offAllNamed(RootScreen.pageId);
    } catch (e) {
      printLog(e);
      Helpers.showSnackbar(title: 'Error', message: e.message);
    }
  }

  Stream<QuerySnapshot> getUser() {
    //status.value = Status.LOADING;
    //   isLoading.value = true;
  }

  saveLoginData(String email, String password) {
    if (radioValue.value != 0)
      Get.find<NotificationController>().fcmSubscribe();
    MySharedPreference.setBoolean(SharedPrefKey.ISLOGIN, true);
    MySharedPreference.setString(SharedPrefKey.EMAIL, email);
    MySharedPreference.setString(SharedPrefKey.PASSWORD, password);
    radioValue.value == 0
        ? MySharedPreference.setBoolean(SharedPrefKey.ISUSER, true)
        : MySharedPreference.setBoolean(SharedPrefKey.ISUSER, false);
    printLog("radioValue.value..........................${radioValue.value}");
    isLoading.value = false;
    status.value = Status.SUCCESS;
    Get.offNamed(HomeScreen.pageId);
  }
}
