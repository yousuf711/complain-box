import 'package:complain_box/src/model/complain_model.dart';
import 'package:complain_box/src/model/user_model.dart';
import 'package:complain_box/src/repo/home_service.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:get/get.dart';

class NotificationController extends GetxController {
  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin;

  FirebaseMessaging _firebaseMessaging;

  @override
  void onInit() {
    _firebaseMessaging = FirebaseMessaging();
    flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();
    var initializationSettingsAndroid =
        AndroidInitializationSettings('@mipmap/ic_launcher');

    var initializationSettings =
        InitializationSettings(android: initializationSettingsAndroid);
    flutterLocalNotificationsPlugin.initialize(initializationSettings,
        onSelectNotification: selectNotification);

    _firebaseMessaging.configure(
      onBackgroundMessage: myBackgroundHandler,
      onMessage: (Map<String, dynamic> message) async {
        print(
            "onMessage............................................: $message");

        AlertDialog(
          title: Text("New Notification"),
          content: Text(
              // notification
              'i want ${message['notification']['title']} for ${message['notification']['body']}'),
          actions: <Widget>[
            FlatButton(
              child: Text('Ok'),
              onPressed: () {
                Get.back();
              },
            ),
          ],
        );
      },
    );

    getTokenz();
    super.onInit();
  }

  getTokenz() async {
    String token = await _firebaseMessaging.getToken();
    print(token);
    //  FirebaseMessaging().subscribeToTopic('technician');
  }

  Future selectNotification(String payload) async {
    await flutterLocalNotificationsPlugin.cancelAll();
  }

  sendNotification(ComplainModel complainModel, UserModel userModel) async {
    Map<String, dynamic> bodyMap = <String, dynamic>{
      'body': complainModel.description,
      'title': complainModel.title,
    //  'isImage': 'no',
    //  'sender_phone': userModel.phone
    };
    Map<String, dynamic> notificationMap = <String, dynamic>{
      'to': '/topics/technician',
      'collapse_key': 'type_a',
      'notification': bodyMap,
      'data': bodyMap
    };
    await new ComplainService().sendNotification(notificationMap);
  }

//  sendNotification(ComplainModel complainModel) {
//    Map<String, dynamic> bodyMap = <String, dynamic>{
//      'body': "Notification body From Api",
//      'title': 'Notification title From Api'
////      'isImage': 'no',
////      'sender_phone': '01837005336',
////      'receiver_phone': '01821821971'
//    };
//    Map<String, dynamic> notificationMap = <String, dynamic>{
//      'to': '/topics/technician',
//      'collapse_key': 'type_a',
//      'notification': bodyMap,
//      'data': bodyMap
//    };
//
//
//  }

  Future _showNotification(Map<String, dynamic> message) async {
    var androidPlatformChannelSpecifics = new AndroidNotificationDetails(
      'channel id',
      'channel name',
      'channel desc',
      importance: Importance.max,
      priority: Priority.high,
    );
    print(
        'messge................................................................................ background');
    var platformChannelSpecifics =
        new NotificationDetails(android: androidPlatformChannelSpecifics);
    await flutterLocalNotificationsPlugin.show(
      0,
      'new message arived',
      'i want ${message['notification']['title']} for ${message['notification']['price']}',
      platformChannelSpecifics,
      payload: 'Default_Sound',
    );
  }

  void fcmSubscribe() {
    _firebaseMessaging.subscribeToTopic('technician');
  }

  void fcmUnSubscribe() {
     _firebaseMessaging.unsubscribeFromTopic('technician');
  }
}

Future<dynamic> myBackgroundHandler(Map<String, dynamic> message) {
  return NotificationController()._showNotification(message);
}
