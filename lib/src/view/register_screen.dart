import 'package:complain_box/src/controller/auth_controller.dart';
import 'package:complain_box/src/helping/appStringFile.dart';
import 'package:complain_box/src/mixins/mixins.dart';
import 'package:complain_box/src/view/login_screen.dart';
import 'package:complain_box/src/widget/auto_size_text_widget.dart';
import 'package:complain_box/src/widget/commons.dart';
import 'package:complain_box/src/widget/custom_text.dart';
import 'package:complain_box/src/widget/typewriter_widget.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:loading_overlay/loading_overlay.dart';

class RegisterScreen extends StatelessWidget with PrintLogMixin {
  static const pageId = 'register_screen';

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final TextEditingController _emailInputCtrl = TextEditingController();
  final TextEditingController _phoneInputCtrl = TextEditingController();
  final TextEditingController _nameInputCtrl = TextEditingController();
  final TextEditingController _passwordInputCtrl = TextEditingController();
  final TextEditingController _conPasswordInputCtrl = TextEditingController();

  final AuthController _authCtrl = Get.find();

  handleFormSubmit() async {
    if (this._formKey.currentState.validate()) {
      printLog(this._formKey.currentState);
      await _authCtrl.register(
          _phoneInputCtrl.text.trim().toString(),
          _emailInputCtrl.text.trim().toString(),
          _nameInputCtrl.text.trim().toString(),
          _passwordInputCtrl.text.trim().toString());
    }
  }

  handleFocus(BuildContext context) {
    FocusScopeNode currentFocus = FocusScope.of(context);
    if (!currentFocus.hasPrimaryFocus) {
      currentFocus.unfocus();
    }
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        handleFocus(context);
      },
      child: Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
          title: CustomText(
            text: AppStringKey.registration.tr,
            size: 18,
            color: white,
          ),
          centerTitle: true,
        ),
//        extendBodyBehindAppBar: true,
//        extendBody: false,
        body: Obx(
          () => LoadingOverlay(
              child: SingleChildScrollView(
                child: Padding(
                  padding: const EdgeInsets.all(24.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Container(
                        child: Form(
                          key: _formKey,
                          child: Column(
                            children: [
                              TypewriterWidget(
                                onTap: () {
                                  handleFocus(context);
                                },
                                textList: [AppStringKey.user_registation.tr],
                                textStyle: TextStyle(
                                    fontSize: 25.0,
                                    color: red,
                                    fontWeight: FontWeight.bold),
                              ),
                              SizedBox(
                                height: 30.0,
                              ),
                              TextFormField(
                                keyboardType: TextInputType.text,
                                decoration: buildFormInputDecoration(
                                    hintText: AppStringKey.name.tr,
                                    icon: Icons.drive_file_rename_outline),
                                controller: _nameInputCtrl,
                                validator: (name) => (name.trim().length) < 1
                                    ? 'Please enter your name'
                                    : null,
                              ),
                              SizedBox(
                                height: 10.0,
                              ),
                              TextFormField(
                                keyboardType: TextInputType.phone,
                                decoration: buildFormInputDecoration(
                                    hintText: AppStringKey.phone_number.tr,
                                    icon: Icons.phone),
                                controller: _phoneInputCtrl,
                                validator: (phone) => (phone.trim().length) < 11
                                    ? 'Please enter a valid mobile number'
                                    : null,
                              ),
                              SizedBox(
                                height: 10.0,
                              ),
                              TextFormField(
                                keyboardType: TextInputType.emailAddress,
                                decoration: buildFormInputDecoration(
                                    hintText: AppStringKey.email.tr,
                                    icon: Icons.email),
                                controller: _emailInputCtrl,
                                validator: (email) =>
                                    (!email.trim().contains('@'))
                                        ? 'Please enter a valid email address'
                                        : null,
                              ),
                              SizedBox(
                                height: 10.0,
                              ),
                              TextFormField(
                                keyboardType: TextInputType.visiblePassword,
                                decoration: buildFormInputDecoration(
                                    hintText: AppStringKey.password.tr,
                                    icon: Icons.lock),
                                controller: _passwordInputCtrl,
                                validator: (password) => (password
                                            .trim()
                                            .length <
                                        6)
                                    ? 'Password must be at least 6 characters long'
                                    : null,
                                obscureText: true,
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              TextFormField(
                                keyboardType: TextInputType.visiblePassword,
                                decoration: buildFormInputDecoration(
                                    hintText: AppStringKey.re_password.tr,
                                    icon: Icons.paste_sharp),
                                controller: _conPasswordInputCtrl,
                                validator: (rePassword) => (rePassword.trim() !=
                                        _passwordInputCtrl.text.trim())
                                    ? 'password mismatch'
                                    : null,
                              ),
                              FlatButton(
                                onPressed: handleFormSubmit,
                                child: Container(
                                  margin:
                                      EdgeInsets.symmetric(horizontal: 15.0),
                                  padding: EdgeInsets.all(10.0),
                                  alignment: Alignment.center,
                                  decoration:
                                      buildFlatButtonConatinerDecoration(
                                          color:
                                              Theme.of(context).primaryColor),
                                  child: AutoSizeTextWidget(
                                    text: 'Sign up',
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 18.0,
                                      fontWeight: FontWeight.w600,
                                      letterSpacing: 1.2,
                                    ),
                                  ),
                                ),
                              ),
                              SizedBox(
                                height: 10.0,
                              ),
                              Divider(
                                thickness: 1,
                              ),
                              FlatButton(
                                onPressed: () =>
                                    Get.offAllNamed(LoginScreen.pageId),
                                child: Container(
                                  padding: EdgeInsets.all(5.0),
                                  alignment: Alignment.center,
                                  child: AutoSizeTextWidget(
                                    text: 'Already have an account? Click here.'
                                        .tr,
                                    style: TextStyle(
                                      color: Colors.black54,
                                      fontSize: 14.0,
                                      fontWeight: FontWeight.w600,
                                      letterSpacing: 1.2,
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              isLoading: _authCtrl.isLoading.value),
        ),
      ),
    );
  }

  BoxDecoration buildFlatButtonConatinerDecoration({Color color}) {
    return BoxDecoration(
      color: color,
      borderRadius: BorderRadius.circular(10.0),
      boxShadow: [
        BoxShadow(
          color: Colors.grey,
          blurRadius: 3.0,
          offset: Offset(0, 2),
        ),
      ],
    );
  }

  InputDecoration buildFormInputDecoration({String hintText, IconData icon}) {
    return InputDecoration(
      contentPadding: EdgeInsets.symmetric(vertical: 15.0),
      fillColor: Colors.white70,
      filled: true,
      hintText: '$hintText',
      prefixIcon: Icon(icon),
    );
  }
}
