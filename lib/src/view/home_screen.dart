import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:complain_box/src/controller/auth_controller.dart';
import 'package:complain_box/src/controller/home_controller.dart';
import 'package:complain_box/src/helping/appStringFile.dart';
import 'package:complain_box/src/mixins/mixins.dart';
import 'package:complain_box/src/model/complain_model.dart';
import 'package:complain_box/src/util/my_shared_preparence.dart';
import 'package:complain_box/src/util/shared_pref_key.dart';
import 'package:complain_box/src/util/status.dart';
import 'package:complain_box/src/view/complain_screen.dart';
import 'package:complain_box/src/widget/Complain_widget.dart';
import 'package:complain_box/src/widget/auto_size_text_widget.dart';
import 'package:complain_box/src/widget/commons.dart';
import 'package:complain_box/src/widget/complain_count_widget.dart';
import 'package:complain_box/src/widget/custom_text.dart';
import 'package:complain_box/src/widget/loading.dart';
import 'package:complain_box/src/widget/typewriter_widget.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:loading_overlay/loading_overlay.dart';

import 'login_screen.dart';

class HomeScreen extends StatelessWidget with PrintLogMixin {
  static const pageId = 'home_screen';

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final TextEditingController _titleInputCtrl = TextEditingController();
  final TextEditingController _descriptionInputCtrl = TextEditingController();

  final HomeController _homeCtrl = Get.find();
  final AuthController _authCtrl = Get.find();
 // DateFormat formattedDate = DateFormat('yyyy-MM-dd – kk:mm');

  handleFormSubmit() async {
    if (this._formKey.currentState.validate()) {
      printLog(this._formKey.currentState);

      ComplainModel com = ComplainModel(
          userId: _homeCtrl.fireAuth.currentUser.uid,
          title: _titleInputCtrl.text.trim().toString(),
          description: _descriptionInputCtrl.text.toString(),
          dateTime: DateTime.now().toString());
      await _homeCtrl.addComplain(com);
      if (_homeCtrl.status.value == Status.SUCCESS) {
        _titleInputCtrl.clear();
        _descriptionInputCtrl.clear();
      }
    }
  }

  handleFocus(BuildContext context) {
    FocusScopeNode currentFocus = FocusScope.of(context);
    if (!currentFocus.hasPrimaryFocus) {
      currentFocus.unfocus();
    }
  }

  @override
  Widget build(BuildContext context) {
    _homeCtrl.getUser();
    MySharedPreference.getBoolean(SharedPrefKey.ISUSER)
        .then((value) {
      _homeCtrl.isUser.value = value;
//      if(value){
//        _homeCtrl.status.value=Status.SUCCESS;
//      }else{
//        _homeCtrl.status.value=Status.SUCCESS;
//      }
    });
    print(
        'user.........................................................${_homeCtrl
            .isUser.value}');
    return Container(
      child: Obx(
            () =>
            Scaffold(
              key: _scaffoldKey,
              appBar: AppBar(
                title: CustomText(
                  text: AppStringKey.complainbox.tr,
                  size: 18,
                  color: white,
                ),
                centerTitle: true,
              ),
              drawer: Drawer(
                // Add a ListView to the drawer. This ensures the user can scroll
                // through the options in the drawer if there isn't enough vertical
                // space to fit everything.
                child: ListView(
                  // Important: Remove any padding from the ListView.
                  padding: EdgeInsets.zero,
                  children: <Widget>[
                    DrawerHeader(
                      decoration: BoxDecoration(
                        color: Colors.blue,
                      ),
                      child: Wrap(
                        direction: Axis.vertical,
                        alignment: WrapAlignment.center,
                        spacing: 2.0,
                        runAlignment: WrapAlignment.start,
                        runSpacing: 0.0,
                        crossAxisAlignment: WrapCrossAlignment.start,
                        verticalDirection: VerticalDirection.down,
                        children: [
                          Wrap(
                            direction: Axis.horizontal,
                            alignment: WrapAlignment.center,
                            children: [
                              CustomText(
                                text: '${AppStringKey.name.tr} : ',
                                color: white,
                                size: 20,
                                weight: FontWeight.bold,
                              ),
                              CustomText(
                                text: _homeCtrl.userModel.value != null
                                    ? '${_homeCtrl.userModel.value.name}'
                                    : "",
                                color: white,
                                size: 20, weight: FontWeight.bold,
                              )
                            ],
                          ),
                          Wrap(
                            direction: Axis.horizontal,
                            alignment: WrapAlignment.center,
                            children: [
                              CustomText(
                                text: '${AppStringKey.email.tr} : ',
                                color: grey[400],
                                size: 15, weight: FontWeight.bold,
                              ),
                              CustomText(
                                text: _homeCtrl.userModel.value != null
                                    ? '${_homeCtrl.userModel.value.email}'
                                    : "",
                                color: grey[400],
                                size: 15, weight: FontWeight.bold,
                              )
                            ],
                          ),
                          Wrap(
                            direction: Axis.horizontal,
                            alignment: WrapAlignment.center,
                            children: [
                              CustomText(
                                text: '${AppStringKey.phone_number.tr} : ',
                                color: grey[400],
                                size: 15, weight: FontWeight.bold,
                              ),
                              CustomText(
                                text: _homeCtrl.userModel.value != null
                                    ? '${_homeCtrl.userModel.value.phone}'
                                    : "",
                                color: grey[400],
                                size: 15, weight: FontWeight.bold,
                              )
                            ],
                          ),
                        ],
                      ),
                    ),
                    ListTile(
                      leading: Icon(Icons.logout),
                      title: CustomText(
                        text: AppStringKey.logout.tr,
                        color: red,
                        size: 18,
                      ),
                      onTap: () {
                        _authCtrl.signOutUser();
                        // Then close the drawer
                        Navigator.pop(context);
                      },
                    ),
                    if(_homeCtrl.isUser.value) ListTile(
                      leading: Icon(Icons.compare_sharp),
                      title: CustomText(
                        text: AppStringKey.my_complain.tr,
                        color: green,
                        size: 18,
                      ),
                      onTap: () {
                        Get.toNamed(ComplainScreen.pageId);
                      },
                    ),
                  ],
                ),
              ),
              body: _homeCtrl.isUser.value == true
                  ? LoadingOverlay(
                  child: userWidget(context),
                  isLoading: _homeCtrl.isLoading.value)
                  : _technician(),
            ),
      ),
    );
  }

  InputDecoration buildFormInputDecoration({String hintText, IconData icon}) {
    return InputDecoration(
      contentPadding: EdgeInsets.symmetric(vertical: 15.0, horizontal: 5),
      border: new OutlineInputBorder(
        borderRadius: const BorderRadius.all(
          const Radius.circular(10.0),
        ),
      ),
      fillColor: Colors.white70,
      filled: false,
      hintText: '$hintText',
      //  prefixIcon: Icon(icon),
    );
  }

  Widget userWidget(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        height: MediaQuery
            .of(context)
            .size
            .height,
        width: MediaQuery
            .of(context)
            .size
            .width,
        padding: EdgeInsets.all(10),
        decoration: BoxDecoration(
          color: white,
        ),
        child: Form(
          key: _formKey,
          child: Column(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              SizedBox(
                height: 10,
              ),
              TypewriterWidget(
                onTap: () {
                  handleFocus(context);
                },
                textList: [AppStringKey.leave_your_complain_here.tr],
                textStyle: TextStyle(
                    fontSize: 20.0,
                    color: Colors.green,
                    fontWeight: FontWeight.bold),
              ),
              SizedBox(
                height: 30.0,
              ),
              TextFormField(
                keyboardType: TextInputType.text,
                decoration: buildFormInputDecoration(
                    hintText: AppStringKey.title.tr,
                    icon: Icons.drive_file_rename_outline),
                controller: _titleInputCtrl,
                validator: (title) =>
                (title
                    .trim()
                    .length < 1) ? 'title can not be empty' : null,
              ),
              SizedBox(
                height: 10,
              ),
              Container(
                height: 200,
                child: TextFormField(
                  maxLength: 400,
                  maxLines: 10,
                  keyboardType: TextInputType.text,
                  decoration: buildFormInputDecoration(
                      hintText: AppStringKey.description.tr,
                      icon: Icons.drive_file_rename_outline),
                  controller: _descriptionInputCtrl,
                  validator: (description) =>
                  (description
                      .trim()
                      .length < 1)
                      ? 'Description is required'
                      : null,
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Container(
                child: Center(
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: RaisedButton(
                      onPressed: () {
                        FocusScope.of(context).unfocus();
                        handleFormSubmit();
                      },
//
//                                () {
////                              RegistrationModel registration=new RegistrationModel(_firstNameInputCtrl.text.trim().toString(),
////                                  _lastNameInputCtrl.text.trim().toString(), "88"+_phoneInputCtrl.text.trim().toString(),
////                                  _passwordInputCtrl.text.trim().toString(), _emailInputCtrl.text.trim().toString());
////                              Get.find<RegistrationController>().registration(registration);
//
//                            },
                      //                   ,
                      color: green[800],
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: CustomText(
                          text: AppStringKey.sendComplain.tr,
                          color: white,
                        ),
                      ),
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: 20,
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _technician() {
    return Column(
      children: [
//        Container(
//          padding: EdgeInsets.only(top: 90, left: 30, right: 30, bottom: 30),
//          child: Column(
//            mainAxisAlignment: MainAxisAlignment.start,
//            crossAxisAlignment: CrossAxisAlignment.start,
//            children: [
//              Container(
//                child: Row(
//                  children: [
//                    GestureDetector(
//                      onTap: () {
//                        _scaffoldKey.currentState.openDrawer();
//                      },
//                      child: CircleAvatar(
//                        radius: 25,
//                        backgroundColor: Colors.white,
//                        child: Icon(
//                          Icons.menu,
//                          color: Colors.black54,
//                          size: 30,
//                        ),
//                      ),
//                    ),
//                    SizedBox(
//                      width: 20,
//                    ),
//                    Expanded(
//                      child: Column(
//                        children: [
//                          AutoSizeTextWidget(
//                            text: AppStringKey.all_complain.tr,
//                            style: TextStyle(
//                              color: Colors.black,
//                              fontSize: 36,
//                              fontWeight: FontWeight.w600,
//                            ),
//                          ),
//                          GetX<HomeController>(
//                            builder: (_) => ContactsCountWidget(
//                              complainCount: _?.complainListCount?.value,
//                            ),
//                          ),
//                        ],
//                      ),
//                    ),
//                    IconButton(
//                      icon: Icon(Icons.dangerous),
//                      iconSize: 30,
//                      onPressed: () {
//                        // Get.toNamed(TestScreen.pageId);
//                      },
//                    ),
//                  ],
//                ),
//              ),
//            ],
//          ),
//        ),
        Expanded(
          child: Container(
            // padding: EdgeInsets.symmetric(horizontal: 5),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(15),
                topRight: Radius.circular(15),
              ),
              boxShadow: [
                BoxShadow(
                  color: Colors.grey,
                  blurRadius: 6.0,
                  offset: Offset(0, -2),
                  spreadRadius: 0.5,
                )
              ],
            ),
            child: StreamBuilder<QuerySnapshot>(
              stream: Get.find<HomeController>().fetchComplain(),
              builder: (context, stream) {
                if (stream.connectionState == ConnectionState.waiting) {
                  return Center(child: CircularProgressIndicator());
                }

                if (stream.hasError) {
                  return Center(child: Text(stream.error.toString()));
                }

                QuerySnapshot querySnapshot = stream.data;

                return ClipRRect(
//                    borderRadius: BorderRadius.only(
//                      topLeft: Radius.circular(15),
//                      topRight: Radius.circular(15),
//                    ),
                  child: Container(
                    child: ListView.separated(
                      padding: EdgeInsets.only(top: 30, bottom: 20),
                      physics: BouncingScrollPhysics(),
                      itemCount: querySnapshot.size,
                      separatorBuilder: (context, index) => Divider(),
                      itemBuilder: (context, index) {
                        final item = querySnapshot.docs[index];
                        // print(item.id);
                        final ComplainModel complainItem =
                        ComplainModel.fromQueryDocumentSnapshot(
                            queryDocSnapshot: item);

                        return complainItem.description
                            .trim()
                            .length > 0 ? ComplainWidget(complainItem) : Text(
                            '');
                      },
                    ),
                  ),
                );
              },
            ),
          ),
        ),
      ],
    );
  }
}
