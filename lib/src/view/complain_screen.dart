import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:complain_box/src/controller/home_controller.dart';
import 'package:complain_box/src/helping/appStringFile.dart';
import 'package:complain_box/src/model/complain_model.dart';
import 'package:complain_box/src/widget/Complain_widget.dart';
import 'package:get/get.dart';
import 'package:flutter/material.dart';

class ComplainScreen extends StatelessWidget {
  static const pageId = 'complain_screen';

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  handleFocus(BuildContext context) {
    FocusScopeNode currentFocus = FocusScope.of(context);
    if (!currentFocus.hasPrimaryFocus) {
      currentFocus.unfocus();
    }
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
          title: Text(AppStringKey.my_complain.tr),
          centerTitle: true,
        ),
        body:  _technician(),
      ),

//
//      Obx(
//        () =>
//      ),
    );
  }

  Widget _technician() {
    return Column(
      children: [
//        Container(
//          padding: EdgeInsets.only(top: 90, left: 30, right: 30, bottom: 30),
//          child: Column(
//            mainAxisAlignment: MainAxisAlignment.start,
//            crossAxisAlignment: CrossAxisAlignment.start,
//            children: [
//              Container(
//                child: Row(
//                  children: [
//                    GestureDetector(
//                      onTap: () {
//                        _scaffoldKey.currentState.openDrawer();
//                      },
//                      child: CircleAvatar(
//                        radius: 25,
//                        backgroundColor: Colors.white,
//                        child: Icon(
//                          Icons.menu,
//                          color: Colors.black54,
//                          size: 30,
//                        ),
//                      ),
//                    ),
//                    SizedBox(
//                      width: 20,
//                    ),
//                    Expanded(
//                      child: Column(
//                        children: [
//                          AutoSizeTextWidget(
//                            text: AppStringKey.all_complain.tr,
//                            style: TextStyle(
//                              color: Colors.black,
//                              fontSize: 36,
//                              fontWeight: FontWeight.w600,
//                            ),
//                          ),
//                          GetX<HomeController>(
//                            builder: (_) => ContactsCountWidget(
//                              complainCount: _?.complainListCount?.value,
//                            ),
//                          ),
//                        ],
//                      ),
//                    ),
//                    IconButton(
//                      icon: Icon(Icons.dangerous),
//                      iconSize: 30,
//                      onPressed: () {
//                        // Get.toNamed(TestScreen.pageId);
//                      },
//                    ),
//                  ],
//                ),
//              ),
//            ],
//          ),
//        ),
        Expanded(
          child: Container(
            // padding: EdgeInsets.symmetric(horizontal: 5),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(15),
                topRight: Radius.circular(15),
              ),
              boxShadow: [
                BoxShadow(
                  color: Colors.grey,
                  blurRadius: 6.0,
                  offset: Offset(0, -2),
                  spreadRadius: 0.5,
                )
              ],
            ),
            child: StreamBuilder<QuerySnapshot>(
              stream: Get.find<HomeController>().fetchMyComplain(),
              builder: (context, stream) {
                if (stream.connectionState == ConnectionState.waiting) {
                  return Center(child: CircularProgressIndicator());
                }

                if (stream.hasError) {
                  return Center(child: Text(stream.error.toString()));
                }
                QuerySnapshot querySnapshot = stream.data;
                return ClipRRect(
//                    borderRadius: BorderRadius.only(
//                      topLeft: Radius.circular(15),
//                      topRight: Radius.circular(15),
//                    ),
                  child: Container(
                    child: ListView.separated(
                      padding: EdgeInsets.only(top: 30, bottom: 20),
                      physics: BouncingScrollPhysics(),
                      itemCount: querySnapshot.size,
                      separatorBuilder: (context, index) => Divider(),
                      itemBuilder: (context, index) {
                        final item = querySnapshot.docs[index];
                        // print(item.id);
                        final ComplainModel complainItem =
                            ComplainModel.fromQueryDocumentSnapshot(
                                queryDocSnapshot: item);

                        //   return Text('Hello');

                        return complainItem.description.trim().length > 0
                            ? ComplainWidget(complainItem)
                            : Text('');
                      },
                    ),
                  ),
                );
              },
            ),
          ),
        ),
      ],
    );
  }
}
