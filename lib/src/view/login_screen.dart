import 'package:complain_box/src/controller/auth_controller.dart';
import 'package:complain_box/src/controller/home_controller.dart';
import 'package:complain_box/src/helping/appStringFile.dart';
import 'package:complain_box/src/mixins/mixins.dart';
import 'package:complain_box/src/view/register_screen.dart';
import 'package:complain_box/src/widget/commons.dart';
import 'package:complain_box/src/widget/custom_text.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:loading_overlay/loading_overlay.dart';


class LoginScreen extends StatelessWidget with PrintLogMixin {
  static const pageId = 'loginscreen';
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  TextEditingController _userNameController = TextEditingController();
  TextEditingController _passwordController = TextEditingController();

  final AuthController _authCtrl = Get.find();


  handleFormSubmit() async {
    if (this._formKey.currentState.validate()) {
      printLog(this._formKey.currentState);
      await _authCtrl.login(_userNameController.text, _passwordController.text);
      //Get.find<LoginController>().login(_userNameController.text.trim().toString(),_passwordController.text.trim().toString());
    }
  }

  handleFocus(BuildContext context) {
    FocusScopeNode currentFocus = FocusScope.of(context);
    if (!currentFocus.hasPrimaryFocus) {
      currentFocus.unfocus();
    }
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        handleFocus(context);
      },
      child: Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
          title: CustomText(
            text: AppStringKey.login.tr, size: 18, color: white,),
          centerTitle: true,
        ),
        body: Obx(
              () =>
              LoadingOverlay(
                  child: SingleChildScrollView(
                    child: Container(
                      height: MediaQuery
                          .of(context)
                          .size
                          .height,
                      width: MediaQuery
                          .of(context)
                          .size
                          .width,
                      padding: EdgeInsets.all(10),
                      decoration: BoxDecoration(
                        color: white,
                      ),
                      child: Form(
                        key: _formKey,
                        child: Column(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            SizedBox(height: 10,),
                            Image.asset(
                              'assets/images/complain.png', height: 100,
                              width: 100,),
                            SizedBox(height: 30,),
                            TextFormField(
                              keyboardType: TextInputType.text,
                              decoration: buildFormInputDecoration(
                                  hintText: AppStringKey.mobile_number.tr,
                                  icon: Icons.drive_file_rename_outline),
                              controller: _userNameController,
                              validator: (useName) =>
                              (useName
                                  .trim()
                                  .length < 1)
                                  ? 'user name is required'
                                  : null,
                            ),
                            SizedBox(height: 10,),
                            TextFormField(
                              keyboardType: TextInputType.text,
                              decoration: buildFormInputDecoration(
                                  hintText: AppStringKey.password.tr,
                                  icon: Icons.drive_file_rename_outline),
                              controller: _passwordController,
                              validator: (password) =>
                              (password
                                  .trim()
                                  .length < 1)
                                  ? 'password is required'
                                  : null,
                            ),
                            SizedBox(height: 10,),
                            Row(
                              children: [
                                Radio(
                                  value: 0,
                                  groupValue: _authCtrl.radioValue.value,
                                  onChanged: _authCtrl.handleRadioValueChange,
                                ),
                                new Text(
                                  'User',
                                  style: new TextStyle(fontSize: 16.0),
                                ),
                                new Radio(
                                  value: 1,
                                  groupValue: _authCtrl.radioValue.value,
                                  onChanged: _authCtrl.handleRadioValueChange,
                                ),
                                new Text(
                                  'Technician',
                                  style: new TextStyle(
                                    fontSize: 16.0,
                                  ),
                                ),
                              ],
                            ),

                            SizedBox(height: 10,),
                            Container(
                              child: Center(
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: RaisedButton(
                                    onPressed: () {
                                      FocusScope.of(context).unfocus();
                                      handleFormSubmit();
                                    },
//
//                                () {
////                              RegistrationModel registration=new RegistrationModel(_firstNameInputCtrl.text.trim().toString(),
////                                  _lastNameInputCtrl.text.trim().toString(), "88"+_phoneInputCtrl.text.trim().toString(),
////                                  _passwordInputCtrl.text.trim().toString(), _emailInputCtrl.text.trim().toString());
////                              Get.find<RegistrationController>().registration(registration);
//
//                            },
                                    //                   ,
                                    color: green[800],
                                    child: Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: CustomText(
                                        text: AppStringKey.action_login.tr,
                                        color: white,
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(height: 20,),
                            GestureDetector(
                              onTap: () {
                                Get.toNamed(RegisterScreen.pageId);
                              },
                              child: RichText(
                                text: TextSpan(
                                  text: AppStringKey.new_account.tr,
                                  style: TextStyle(fontWeight: FontWeight.bold,
                                      color: black,
                                      fontSize: 18),
                                  children: <TextSpan>[
                                    TextSpan(text: AppStringKey.registration.tr,
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            color: red,
                                            fontSize: 18)),
                                  ],
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                  ), isLoading: _authCtrl.isLoading.value),
        ),
      ),
    );
  }

  InputDecoration buildFormInputDecoration({String hintText, IconData icon}) {
    return InputDecoration(
      contentPadding: EdgeInsets.symmetric(vertical: 15.0),
      border: new OutlineInputBorder(
        borderRadius: const BorderRadius.all(
          const Radius.circular(10.0),
        ),
      ),
      fillColor: Colors.white70,
      filled: false,
      hintText: '$hintText',
      prefixIcon: Icon(icon),
    );
  }
}
