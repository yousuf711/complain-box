
import 'package:complain_box/src/helping/appStringFile.dart';
import 'package:complain_box/src/view/home_screen.dart';
import 'package:complain_box/src/view/login_screen.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'package:splashscreen/splashscreen.dart';

//
//
class MySplashScreen extends StatelessWidget {
  static const pageId = 'splash_screen';

  @override
  Widget build(BuildContext context) {
    return  SplashScreen(
        seconds: 2,
        navigateAfterSeconds: FirebaseAuth.instance.currentUser==null? new LoginScreen():new HomeScreen(),
        title: new Text(
          AppStringKey.app_name.tr,
          style: new TextStyle(fontWeight: FontWeight.bold, fontSize: 20.0),
        ),
        image: new Image.asset('assets/images/complain.png',height: 80,width: 80,),
      //  image: new Image.network('https://i.pinimg.com/564x/fc/c5/77/fcc57757270fbcacb3ec70a4ec384d26.jpg'),
        backgroundColor: Colors.white,
        styleTextUnderTheLoader: new TextStyle(),
        useLoader: false,
        photoSize: 100.0);
      //  onClick: () => print("Flutter Egypt"),
      //  loaderColor: Colors.red);
  }
}
