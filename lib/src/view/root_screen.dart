import 'package:complain_box/src/view/splash_screen.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';


class RootScreen extends StatelessWidget {
  static const pageId = 'root_screen';

  // calling Get.find method to initialize Controller instances
  // final AuthController _authCtrl = Get.find();
  // final ContactController _contactCtrl = Get.find();
  // final UploadController _uploadCtrl = Get.find();

  @override
  Widget build(BuildContext context) {
    return MySplashScreen();
//    return GetX(
//
//      initState: (_) =>
//          Get.put<AuthController>(AuthController(), permanent: true),
//      builder: (_) {
//        if (Get.find<AuthController>().user?.uid != null) {
//          return HomeScreen();
//        } else {
//          return SplashScreen();
//        }
//      },
//    );
  }
}
