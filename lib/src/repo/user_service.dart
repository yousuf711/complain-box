import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:complain_box/src/mixins/mixins.dart';
import 'package:complain_box/src/model/user_model.dart';


class UserService with PrintLogMixin {
  final FirebaseFirestore _firestore = FirebaseFirestore.instance;

  Future<UserModel> createNewUser(UserModel user) async {
    await _firestore.collection("users").doc(user.id).set({
      "id": user.id,
      "name": user.name,
      "email": user.email,
      "phone": user.phone,
      "isUser":true,
    });
    return user;
  }



    Future<DocumentSnapshot> getUser(String userId) async{
     DocumentSnapshot variable = await _firestore.collection('users').doc(userId).get();
return variable;
//    return _firestore
//        .collection("users")
//        .doc(userId.toString());
//     // .snapshots();
//     //   .collection('contacts')
//    //    .snapshots();

  }
}
