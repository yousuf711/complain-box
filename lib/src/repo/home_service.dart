import 'dart:convert';
import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:complain_box/src/api/api_client_fcm.dart';
import 'package:complain_box/src/controller/home_controller.dart';
import 'package:complain_box/src/model/complain_model.dart';
import 'package:complain_box/src/util/static_key.dart';
import 'package:http/http.dart' as http;
import 'package:get/get.dart';

class ComplainService {
  var _fireStore = FirebaseFirestore.instance;

  //...............................................firebse query..............................
  // https://medium.com/firebase-tips-tricks/how-to-use-firebase-queries-in-flutter-361f21005467
  Future<ComplainModel> addComplain(ComplainModel complainModel) async {
    await _fireStore.collection('complainbox').doc().set({
      'userId': Get.find<HomeController>().userModel.value.id,
      'name': Get.find<HomeController>().userModel.value.name,
      'phone': Get.find<HomeController>().userModel.value.phone,
      'title': complainModel.title,
      'description': complainModel.description,
      'date': complainModel.dateTime
    });
    return complainModel;
  }

  Future<dynamic> sendNotification(Map<String, dynamic> bodyMap) async {
    final response = await http.post(StaticKey.BASE_URL_FCM + Apis.fcm,
        headers: {
          "Content-Type": "application/json",
          "Authorization":
              "key=AAAAi7Lu_VQ:APA91bEAcp50VnjCqxc4PB2wBvssPRU6vOcYZnfo9ieWQzLVSkTUDyCFJMGrEX3BccZwsSl0lHnbYi3Dq7fHyFC7mgac2wVStk6r-Hd1P8tUPEQijjKahogZ26Tble15Tw4eopC2oOwf"
        },
        body: jsonEncode(bodyMap));
    final responseJson = jsonDecode(response.body);
    print('notification res ...........................$responseJson');
    return responseJson;
  }

  //...............................................firebse uery..............................
  // https://medium.com/firebase-tips-tricks/how-to-use-firebase-queries-in-flutter-361f21005467

  Stream<QuerySnapshot> fetchComplain() {
    return _fireStore.collection("complainbox").snapshots();
  }

  Stream<QuerySnapshot> fetchMyComplain(String uId) {
    return _fireStore
        .collection("complainbox")
        .where('userId', isEqualTo: uId)
        .snapshots();
  }
}
