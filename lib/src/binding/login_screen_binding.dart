import 'package:complain_box/src/controller/auth_controller.dart';
import 'package:get/get.dart';

class LoginScreenBinding extends Bindings{
  @override
  void dependencies() {
   Get.lazyPut<AuthController>(() => AuthController());
  }
}