
import 'package:complain_box/src/controller/home_controller.dart';
import 'package:get/get.dart';

class HomeScreenBinding extends Bindings{
  @override
  void dependencies() {
   Get.lazyPut<HomeController>(() => HomeController());
  }
}