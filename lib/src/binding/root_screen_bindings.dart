
import 'package:complain_box/src/controller/auth_controller.dart';
import 'package:complain_box/src/controller/home_controller.dart';
import 'package:complain_box/src/controller/notification_controller.dart';
import 'package:get/get.dart';

class RootScreenBindings extends Bindings {
  @override
  void dependencies() {
    // eager-loading authController and ContactController, also marking them as permanent
    // so that the same instance is available on different screens when we call Get.find() method
    Get.put<AuthController>(AuthController(), permanent: true);
    Get.put<HomeController>(HomeController(), permanent: true);
    Get.put<NotificationController>(NotificationController(), permanent: true);
  }
}
