import 'dart:collection';

import 'package:complain_box/src/util/static_key.dart';

import 'package:dio/dio.dart';
import 'package:retrofit/retrofit.dart';


part 'api_client_fcm.g.dart';

///APIs class is for api tags
class Apis {
  static const String fcm ="/fcm/send";

}

@RestApi(baseUrl: StaticKey.BASE_URL_FCM)
abstract class ApiClientFcm {
  factory ApiClientFcm(Dio dio, {String baseUrl}) = _ApiClientFcm;

//
//  @POST("/fcm/send")
//  @Headers(<String, dynamic>{ //Static header
//    "Content-Type" : "application/json",
//    "Custom-Header" : "Your header"
//  })
//  Future<dynamic> sendNotification(@Header("Authorization") String token);
//



}
