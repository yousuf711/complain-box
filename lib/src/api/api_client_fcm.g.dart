// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'api_client_fcm.dart';

// **************************************************************************
// RetrofitGenerator
// **************************************************************************

class _ApiClientFcm implements ApiClientFcm {
  _ApiClientFcm(this._dio, {this.baseUrl}) {
    ArgumentError.checkNotNull(_dio, '_dio');
    baseUrl ??= 'https://fcm.googleapis.com';
  }

  final Dio _dio;

  String baseUrl;

  @override
  Future<dynamic> sendNotification() async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _data = <String, dynamic>{};
    final _result = await _dio.request('/fcm/send',
        queryParameters: queryParameters,
        options: RequestOptions(
            method: 'POST',
            headers: <String, dynamic>{},
            extra: _extra,
            baseUrl: baseUrl),
        data: _data);
    final value = _result.data;
    return value;
  }
}
