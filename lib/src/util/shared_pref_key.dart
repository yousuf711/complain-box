


class SharedPrefKey{
  static final USER_ID='user_id';
  static final PHONE_NUMBER='phonenumber';
  static final EMAIL='email';
  static final PASSWORD='password';
  static final ISLOGIN='islogin';
  static final ISUSER='isuser';

}