import 'package:shared_preferences/shared_preferences.dart';

class MySharedPreference {
  static void setInt(String key, int value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setInt(key, value);
  }
  static Future<int> getInt(String key) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
   return prefs.getInt(key);
  }
  static void setString(String key, String value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString(key, value);
  }
  static Future<String> getString(String key) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
   return prefs.getString(key);
  }
   static void setBoolean(String key, bool value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool(key, value);
  }
  static Future<bool> getBoolean(String key) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
   return prefs.getBool(key);
  }
}
